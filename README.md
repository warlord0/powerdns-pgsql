# PowerDNS with PowerDNS Admin and PostgreSQL Backend

This docker-compose hosts PowerDNS, the Admin GUI and a PostgreSQL backend.

## Environment

Set the API key and database credentials in `.env` (see [.env.sample](.env.sample)).

```properties
API_KEY=MySuperSecretKey
POSTGRES_USER=postgres
POSTGRES_PASSWORD=MySecretKey
POSTGRES_PDNS_PASSWORD=NoHumanNeedsThis
```

## Installation

Run the db container to start the DB first. You will need to create the database before starting the remaining powerdns and admin containers.

```shell
docker-compose up -d db
```

You may need to get an updated schema from [https://doc.powerdns.com/authoritative/backends/generic-postgresql.html](https://doc.powerdns.com/authoritative/backends/generic-postgresql.html). If you do then edit the init.db.sh file and paste the schema into the second psql section.

Start the remaining containers:

```shell
docker-compose up -d
```

## Connect

By default the admin web page is hosted on port ${PORTBASE}80:

[`http://localhost:${PORTBASE}80`](http://localhost:${PORTBASE}80)

### Settings

When you first connect register to create the user account which will be an admin. When you login with that new account you will be asked to fill in the PowerDNS configuration. You'll need the API key you specified in the `.env`.

The URL for the PowerDNS API is `http://powerdns:8081`

## Notes

The container set uses static IP addressing to ensure they are all able to talk to each other on the same network. It may not be 100% necessary but it made the name resolution easier from the admin to the api to the db.

Although the container for PowerDNS uses command line variables to control the configuration it is also possible to put the values into a `.conf` file under `./conf.d` as this gets mounted and the contained `.conf` files are included at runtime. However, using command line arguments means you can use environment variables. *But,* if it's not in the `.conf` file then using the pdnsutil command to generate a tsig key is problematic as it still tries to use the default mysql :(, eg:

```shell
$ docker-compose exec powerdns pdnsutil generate-tsig-key opusvl hmac-md5

Mar 17 11:23:47 gmysql Connection failed: Unable to connect to database: Can't connect to local MySQL server through socket '/run/mysqld/mysqld.sock' (2)

```

With all the settings in the `.conf` file `pdnsutil` works.

## PostgreSQL Replication

Just use the standard replication process for postgres 12.

### Master

Add WAL settings to the masters `postgresql.conf`, create a `replication` user and entry in `pg_hba.conf`.

#### postgresql.conf

```properties
wal_keep_segments = 80
max_replication_slots = 10
wal_level = 'hot_standby'
archive_mode = on
archive_command = '/bin/true'
max_wal_senders = 5
hot_standby = on
primary_conninfo      = 'host=master port=5432 user=replication'
```

#### Create User

```shell
$ docker-compose exec db createuser -U postgres replication --replication
```

### pg_hba.conf

I added and all for the address because we could be comming from anywhere, but it looks like it sees the NAT'ed address of 172.20.128.1 anyway so is not really relevant as a restriction.

```properties
host    replication     all             all                     trust
```

## Slave

A copy of this container set, but with the data removed from `.pgdata`. If it's on the same host - set a different IP address range, change the postgres and powerdns port. Then create the empty file `standby.signal` to turn it into a standby server.

```shell
docker-compose stop db
docker-compose run --rm db rm -r /var/lib/postgresql/data/
docker-compose run --rm db pg_basebackup -U replication -h master -p 5432 -P -D /var/lib/postgresql/data/
sudo touch .pgdata/standby.signal
docker-compose up -d db
```

On the slave you cannot use the PowerDNS Admin tool or any dynamic updates as the replica is READ ONLY. Keep it in the config and commented out so you can bring it up if you need to promote the slave.

You can use the slave happily as a slave DNS server though and as expected it will respond to queries with a direct replica of the data from the master.
